// Fill out your copyright notice in the Description page of Project Settings.


#include "InteractionPoint.h"
#include "InteractionResult.h"
#include "Components/SphereComponent.h"
#include "PlayerCharacter.h"

// Sets default values
AInteractionPoint::AInteractionPoint()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	InteractionProximity = CreateDefaultSubobject<USphereComponent>(FName(TEXT("InteractionProximity")));
	AddOwnedComponent(InteractionProximity);


}

void AInteractionPoint::UseInteraction()
{
	OnInteraction();

	OnInteractionEvent.Broadcast(this);
}

// Called when the game starts or when spawned
void AInteractionPoint::BeginPlay()
{
	Super::BeginPlay();

	if (InteractionProximity)
	{
		InteractionProximity->OnComponentBeginOverlap.AddDynamic(this, &AInteractionPoint::OnProximityBeginOverlap);
		InteractionProximity->OnComponentEndOverlap.AddDynamic(this, &AInteractionPoint::OnProximityEndOverlap);
	}
	
}

void AInteractionPoint::EndPlay(const EEndPlayReason::Type Reason)
{
	if (InteractionProximity)
	{
		InteractionProximity->OnComponentBeginOverlap.RemoveDynamic(this, &AInteractionPoint::OnProximityBeginOverlap);
		InteractionProximity->OnComponentEndOverlap.RemoveDynamic(this, &AInteractionPoint::OnProximityEndOverlap);
	}
}

void AInteractionPoint::OnProximityBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult)
{
	if (APlayerCharacter* MyCharacter = Cast<APlayerCharacter>(OtherActor))
	{
		MyCharacter->SetInteractionPointTarget(this);
	}
}

void AInteractionPoint::OnProximityEndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	if (APlayerCharacter* MyCharacter = Cast<APlayerCharacter>(OtherActor))
	{
		MyCharacter->SetInteractionPointTarget(nullptr);
	}
}

// Called every frame
void AInteractionPoint::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

