// Fill out your copyright notice in the Description page of Project Settings.


#include "Bullet.h"
#include "EnemyCharacter.h"

// Sets default values
ABullet::ABullet()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ABullet::BeginPlay()
{
	Super::BeginPlay();
	
	OnActorHit.AddDynamic(this, &ABullet::OnHit);

}

void ABullet::EndPlay(const EEndPlayReason::Type e)
{
	OnActorHit.RemoveDynamic(this, &ABullet::OnHit);
}

// Called every frame
void ABullet::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ABullet::OnHit(AActor* SelfActor, AActor* OtherActor, FVector NormalImpulse, const FHitResult& Hit)
{
	if (AEnemyCharacter* Enemy = Cast<AEnemyCharacter>(OtherActor))
	{
		if (Enemy->GetCurrentHealth() < 0)
		{
			Enemy->SetIsDead();
		}
		else
		{
			Enemy->ApplyDamage();
		}
		Destroy();

	}
}