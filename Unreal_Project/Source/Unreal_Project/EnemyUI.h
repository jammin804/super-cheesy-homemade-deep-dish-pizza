// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "EnemyUI.generated.h"

class UImage;

/**
 * 
 */
UCLASS()
class UNREAL_PROJECT_API UEnemyUI : public UUserWidget
{
	GENERATED_BODY()
public:
	UFUNCTION(BlueprintCallable)
	void SetHealthBarPercentage(float percentage);

protected:
	virtual void NativeConstruct() override;

	UPROPERTY(meta=(BindWidgetOptional))
	UImage* HealthBar;

	float MaxHealthBarSize = 0.0f;
	bool HealthBarSizeReceived = false;
};
