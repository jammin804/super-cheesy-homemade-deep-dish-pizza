// Fill out your copyright notice in the Description page of Project Settings.


#include "EnemyUI.h"
#include "Components/CanvasPanelSlot.h"
#include "Components/Image.h"

void UEnemyUI::NativeConstruct()
{
	Super::NativeConstruct();
	if (HealthBar)
	{
		if (UCanvasPanelSlot* PanelSlot = Cast<UCanvasPanelSlot>(HealthBar->Slot))
		{
			MaxHealthBarSize = PanelSlot->GetSize().X;
			HealthBarSizeReceived = true;
		}
		
	}
}

void UEnemyUI::SetHealthBarPercentage(float percentage)
{
	if (!HealthBarSizeReceived)
	{
		return;
	}

	if (HealthBar)
	{
		if (UCanvasPanelSlot* PanelSlot = Cast<UCanvasPanelSlot>(HealthBar->Slot))
		{
			FVector2D NewHealthBarSize = PanelSlot->GetSize();
			NewHealthBarSize.X = MaxHealthBarSize * percentage;
			PanelSlot->SetSize(NewHealthBarSize);
		}
	}
}


