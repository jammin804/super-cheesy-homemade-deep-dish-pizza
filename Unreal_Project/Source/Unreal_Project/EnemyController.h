// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "Perception/AIPerceptionTypes.h"
#include "RenderCore.h"
#include "EnemyController.generated.h"


class UBehaviorTree;
//class FTimer;

/**
 * 
 */
UCLASS()
class UNREAL_PROJECT_API AEnemyController : public AAIController
{
	GENERATED_BODY()
public:

	UFUNCTION(BlueprintCallable)
	void OnPerceptionUpdated(AActor* Actor, const FAIStimulus& Stimulus);
protected:
	virtual void Tick(float DeltaSeconds) override;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	UBehaviorTree* BehaviorTree;
private:
	virtual void OnPossess(APawn* InPawn) override;
	
	/*UPROPERTY(Transient)
	FTimer EnemyTimer;*/
};
