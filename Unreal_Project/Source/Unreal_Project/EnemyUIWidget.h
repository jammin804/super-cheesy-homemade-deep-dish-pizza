// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/WidgetComponent.h"
#include "EnemyUIWidget.generated.h"

/**
 * 
 */
UCLASS()
class UNREAL_PROJECT_API UEnemyUIWidget : public UWidgetComponent
{
	GENERATED_BODY()
	
public:
	void SetHealthBarPct(float HealthBarPercentage);
};
