// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "Perception/AIPerceptionTypes.h"
#include "RenderCore.h"
#include "SidekickController.generated.h"


class UBehaviorTree;
//class FTimer;

/**
 * 
 */
UCLASS()
class UNREAL_PROJECT_API ASidekickController : public AAIController
{
	GENERATED_BODY()
public:

	UFUNCTION(BlueprintCallable)
	void Follow();

	UFUNCTION(BlueprintCallable)
	void Wait();
protected:
	virtual void BeginPlay() override;

	virtual void Tick(float DeltaSeconds) override;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	UBehaviorTree* BehaviorTree;
private:
	virtual void OnPossess(APawn* InPawn) override;

};
