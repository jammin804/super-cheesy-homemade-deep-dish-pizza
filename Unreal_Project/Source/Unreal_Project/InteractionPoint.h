// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Delegates/DelegateCombinations.h"
#include "InteractionPoint.generated.h"

class USphereComponent;
class AInteractionResult;

DECLARE_MULTICAST_DELEGATE_OneParam(FInteractionEvent, AInteractionPoint*);

UCLASS()
class UNREAL_PROJECT_API AInteractionPoint : public AActor
{
	GENERATED_BODY()
	
public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Sets default values for this actor's properties
	AInteractionPoint();

	void UseInteraction();

	
	FInteractionEvent OnInteractionEvent;

	UFUNCTION(BlueprintImplementableEvent)
	void OnInteraction();

	

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	virtual void EndPlay(const EEndPlayReason::Type Reason) override;


	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
	USphereComponent* InteractionProximity;

	UFUNCTION() 
		void OnProximityBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
			UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult);

	UFUNCTION() 
		void OnProximityEndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
			UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

private:
	bool bInsideProximity = false;
};
