// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "EnemyCharacter.generated.h"

class APathPoint;
class UEnemyUIWidget;

UENUM(BlueprintType)
enum class EPatrolBehaviour : uint8
{
	DoNothing,
	PatrolBack,
	Loop
};

UCLASS()
class UNREAL_PROJECT_API AEnemyCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AEnemyCharacter();

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	UFUNCTION(BlueprintCallable)
	void SetIsDead();

	bool IsDead() const { return bIsDead; }

	int GetCurrentHealth() const { return CurrentHealth; }
	
	void ApplyDamage();
protected:
	UPROPERTY(BlueprintReadOnly)
	bool bIsDead = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TArray<APathPoint*> PatrolPath;

	UPROPERTY(VisibleDefaultsOnly, BlueprintReadWrite)
	UEnemyUIWidget* Widget;

	virtual void OnConstruction(const FTransform& Transform) override;

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	virtual void EndPlay(const EEndPlayReason::Type e) override;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	int MaxHealth = 3;

	UPROPERTY(BlueprintReadWrite)
	int CurrentHealth = MaxHealth;

    UPROPERTY(BlueprintReadWrite)
    int CurrentIndexOnPath = 0;

    UPROPERTY(BlueprintReadWrite)
    int CurrentDirectionOnPath = 0;

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    EPatrolBehaviour CurrentBehaviour = EPatrolBehaviour::DoNothing;
private:

	void UpdateHealthBarWidget();

	UFUNCTION()
	void OnHit(AActor* SelfActor, AActor* OtherActor, FVector NormalImpulse, const FHitResult& Hit);
};
