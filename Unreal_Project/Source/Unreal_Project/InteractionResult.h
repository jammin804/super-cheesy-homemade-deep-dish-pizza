// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "InteractionResult.generated.h"

class AInteractionPoint;

UENUM(BlueprintType)
enum class EInteractionTypes : uint8
{
	Standard,
	Simultaneous
};

UCLASS()
class UNREAL_PROJECT_API AInteractionResult : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AInteractionResult();
	UFUNCTION(BlueprintImplementableEvent)
	void OnInteraction();

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION()
		void OnInteractionPointInteracted(AInteractionPoint* InteractionPoint);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	virtual void EndPlay(const EEndPlayReason::Type Reason) override;

	UPROPERTY(EditAnywhere)
	TArray<AInteractionPoint*> InteractionPoints;

	UPROPERTY(EditAnywhere)
	EInteractionTypes InteractionType;

private:
	TArray<FDelegateHandle> InteractionEvents;

	TArray<bool> InteractionsPressed;

	bool bInteractionComplete = false;
};
