// Fill out your copyright notice in the Description page of Project Settings.


#include "SidekickController.h"
#include "BehaviorTree/BlackboardComponent.h"

void ASidekickController::BeginPlay()
{
	Super::BeginPlay();
	Wait();
}

void ASidekickController::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	

}

void ASidekickController::Follow()
{
	
	static FName bIsFollowing(TEXT("IsFollowing?"));

	Blackboard->SetValueAsBool(bIsFollowing, true);


}

void ASidekickController::Wait()
{

	static FName bIsFollowing(TEXT("IsFollowing?"));

	Blackboard->SetValueAsBool(bIsFollowing, false);


}

void ASidekickController::OnPossess(APawn* InPawn)
{
	Super::OnPossess(InPawn);
	if (BehaviorTree)
	{
		RunBehaviorTree(BehaviorTree);
	}

}
