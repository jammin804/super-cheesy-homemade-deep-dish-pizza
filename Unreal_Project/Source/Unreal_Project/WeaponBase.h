// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "WeaponBase.generated.h"

class USphereComponent;

UCLASS()
class UNREAL_PROJECT_API AWeaponBase : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AWeaponBase();

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void ConsumeAmmo();

	void Reload();

	int GetAmmo() const { return Ammo; }

	UFUNCTION(BlueprintCallable)
	void PickupAmmo(int AmountOfAmmo);

	UFUNCTION()
	void OnWeaponDraw(AActor* OverlappedActor, AActor* OtherActor);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	virtual void EndPlay(const EEndPlayReason::Type e) override;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
	USphereComponent* WeaponAttached;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	int MaxAmmo = 100;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	int Ammo = MaxAmmo;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	int ClipSize = 25;


private:
	int MaxRefillAmmo;

};
