// Fill out your copyright notice in the Description page of Project Settings.


#include "InteractionSubsystem.h"
#include "Kismet/GameplayStatics.h"

UInteractionSubsystem* UInteractionSubsystem::Get(const UObject* WorldObject)
{
	UGameInstance* GameInstance = UGameplayStatics::GetGameInstance(WorldObject);
	return GameInstance->GetSubsystem<UInteractionSubsystem>();
}
