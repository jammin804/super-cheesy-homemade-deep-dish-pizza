// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "EnemyUIWidget.h"
#include "MyEnemyUIWidget.generated.h"

/**
 * 
 */
UCLASS()
class UNREAL_PROJECT_API UMyEnemyUIWidget : public UEnemyUIWidget
{
	GENERATED_BODY()
	
public:
	// Sets default values for this character's properties
	UMyEnemyUIWidget();

	// Called every frame
	void NativeTick(const FGeometry& MyGeometry, float InDeltaTime);

protected:

private:

};
