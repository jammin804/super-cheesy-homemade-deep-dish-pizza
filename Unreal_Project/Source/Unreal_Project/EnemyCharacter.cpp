// Fill out your copyright notice in the Description page of Project Settings.


#include "EnemyCharacter.h"
#include "EnemyController.h"
#include "EnemyUIWidget.h"
#include "Kismet/GameplayStatics.h"


// Sets default values
AEnemyCharacter::AEnemyCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

void AEnemyCharacter::OnConstruction(const FTransform& Transform)
{
	Super::OnConstruction(Transform);
}

// Called when the game starts or when spawned
void AEnemyCharacter::BeginPlay()
{
	Super::BeginPlay();
	Widget = FindComponentByClass<UEnemyUIWidget>();
	CurrentHealth = MaxHealth;
	UpdateHealthBarWidget();

	OnActorHit.AddDynamic(this, &AEnemyCharacter::OnHit);
}

void AEnemyCharacter::EndPlay(const EEndPlayReason::Type e)
{
	OnActorHit.RemoveDynamic(this, &AEnemyCharacter::OnHit);
}

void AEnemyCharacter::UpdateHealthBarWidget()
{
    if (Widget)
    {
		float HealthPercentage = CurrentHealth / static_cast<float>(MaxHealth);
		Widget->SetHealthBarPct(HealthPercentage);
    }
}

// Called every frame
void AEnemyCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	if (bIsDead)
	{
		if (AEnemyController* AIController = Cast<AEnemyController>(GetController()))
		{
			UnPossessed();
			SetActorEnableCollision(false);
		}
	}
	else
	{
		UpdateHealthBarWidget();
	}
}

// Called to bind functionality to input
void AEnemyCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}


void AEnemyCharacter::SetIsDead()
{
	bIsDead = true;
	DetachFromControllerPendingDestroy();
	if (Widget)
	{
		Widget->SetVisibility(false);
	}
}

void AEnemyCharacter::ApplyDamage()
{
	if (IsDead())
	{
		return;
	}

	CurrentHealth -= 1;
	if (CurrentHealth <= 0)
	{
		SetIsDead();
	}
}

void AEnemyCharacter::OnHit(AActor* SelfActor, AActor* OtherActor, FVector NormalImpulse, const FHitResult& Hit)
{
	if (!OtherActor)
	{
		return;
	}

	static FName PlayerTag(TEXT("Player"));
	static FName SideKickTag(TEXT("Sidekick"));

	if (OtherActor->ActorHasTag(PlayerTag) || OtherActor->ActorHasTag(SideKickTag))
	{
		UGameplayStatics::OpenLevel(this, FName(UGameplayStatics::GetCurrentLevelName(this)), true, FString(TEXT("Respawn")));
	}
}


