// Fill out your copyright notice in the Description page of Project Settings.


#include "Door.h"
#include "Kismet/GameplayStatics.h"
#include "Components/BoxComponent.h"

// Sets default values
ADoor::ADoor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SceneRoot = CreateDefaultSubobject<USceneComponent>(FName(TEXT("SceneRoot")));
	AddOwnedComponent(SceneRoot);

	RootComponent = SceneRoot;

	PortalTrigger = CreateDefaultSubobject<UBoxComponent>(FName(TEXT("PortalTrigger")));
	AddOwnedComponent(PortalTrigger);

	PortalTrigger->AttachToComponent(SceneRoot,FAttachmentTransformRules::KeepRelativeTransform);
}

// Called when the game starts or when spawned
void ADoor::BeginPlay()
{
	Super::BeginPlay();
	
	if (PortalTrigger)
	{
		PortalTrigger->OnComponentBeginOverlap.AddDynamic(this, &ADoor::OnPortalTriggerOverlap);
	}
	

}

void ADoor::EndPlay(const EEndPlayReason::Type Reason)
{
	if (PortalTrigger)
	{
		PortalTrigger->OnComponentBeginOverlap.RemoveDynamic(this, &ADoor::OnPortalTriggerOverlap);
	}
}

void ADoor::OnPortalTriggerOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult)
{
	if (APlayerCharacter* Player = Cast<APlayerCharacter>(OtherActor))
	{
		if (Player->IsSidekickCharacterClose())
		{
			UGameplayStatics::OpenLevel(this, Destination);
		}

	}

}

// Called every frame
void ADoor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

