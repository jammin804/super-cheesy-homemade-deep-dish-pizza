// Copyright Epic Games, Inc. All Rights Reserved.

#include "PlayerCharacter.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/InputComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/Controller.h"
#include "GameFramework/SpringArmComponent.h"
#include "Components/ChildActorComponent.h"
#include "SidekickCharacter.h"
#include "DrawDebugHelpers.h"
#include "EnemyCharacter.h"
#include "InteractionPoint.h"
#include "WeaponBase.h"
#include "Kismet/GameplayStatics.h"

//////////////////////////////////////////////////////////////////////////
// AUnreal_ProjectCharacter

APlayerCharacter::APlayerCharacter()
{
	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// set our turn rates for input
	BaseTurnRate = 45.f;
	BaseLookUpRate = 45.f;

	// Don't rotate when the controller rotates. Let that just affect the camera.
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Character moves in the direction of input...	
	GetCharacterMovement()->RotationRate = FRotator(0.0f, 540.0f, 0.0f); // ...at this rotation rate
	GetCharacterMovement()->JumpZVelocity = 600.f;
	GetCharacterMovement()->AirControl = 0.2f;

	// Create a camera boom (pulls in towards the player if there is a collision)
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->TargetArmLength = 300.0f; // The camera follows at this distance behind the character	
	CameraBoom->bUsePawnControlRotation = true; // Rotate the arm based on the controller

	// Create a follow camera
	FollowCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("FollowCamera"));
	FollowCamera->SetupAttachment(CameraBoom, USpringArmComponent::SocketName); // Attach the camera to the end of the boom and let the boom adjust to match the controller orientation
	FollowCamera->bUsePawnControlRotation = false; // Camera does not rotate relative to arm


	Weapon = CreateDefaultSubobject<UChildActorComponent>(TEXT("Weapon"));
	Weapon->AttachToComponent(GetMesh(), FAttachmentTransformRules::KeepRelativeTransform, FName(TEXT("hand_rSocket")));
	

	bHolding = false;

	// Note: The skeletal mesh and anim blueprint references on the Mesh component (inherited from Character) 
	// are set in the derived blueprint asset named MyCharacter (to avoid direct content references in C++)
}

//////////////////////////////////////////////////////////////////////////
// Input

void APlayerCharacter::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)	
{
	// Set up gameplay key bindings
	check(PlayerInputComponent);
	/*PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ACharacter::Jump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &ACharacter::StopJumping);*/
	
	PlayerInputComponent->BindAction("Grab", IE_Pressed, this, &APlayerCharacter::GrabHand);
	PlayerInputComponent->BindAction("Crouch", IE_Pressed, this, &APlayerCharacter::ToggleCrouch);
	PlayerInputComponent->BindAction("Rock", IE_Pressed, this, &APlayerCharacter::ThrowRock);
	PlayerInputComponent->BindAction("Interact", IE_Pressed, this, &APlayerCharacter::Interact);
	PlayerInputComponent->BindAction("ToggleWeapon", IE_Pressed, this, &APlayerCharacter::ToggleWeapon);
	PlayerInputComponent->BindAction("FireWeapon", IE_Pressed, this, &APlayerCharacter::FireWeapon);
	PlayerInputComponent->BindAction("AimWeapon", IE_Pressed, this, &APlayerCharacter::AimWeaponPressed);
	PlayerInputComponent->BindAction("AimWeapon", IE_Released, this, &APlayerCharacter::AimWeaponReleased);
	PlayerInputComponent->BindAction("Reload", IE_Pressed, this, &APlayerCharacter::Reload);
	PlayerInputComponent->BindAction("Pause", IE_Pressed, this, &APlayerCharacter::PauseGame);

	PlayerInputComponent->BindAxis("MoveForward", this, &APlayerCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &APlayerCharacter::MoveRight);

	// We have 2 versions of the rotation bindings to handle different kinds of devices differently
	// "turn" handles devices that provide an absolute delta, such as a mouse.
	// "turnrate" is for devices that we choose to treat as a rate of change, such as an analog joystick
	PlayerInputComponent->BindAxis("Turn", this, &APawn::AddControllerYawInput);
	PlayerInputComponent->BindAxis("TurnRate", this, &APlayerCharacter::TurnAtRate);
	PlayerInputComponent->BindAxis("LookUp", this, &APawn::AddControllerPitchInput);
	PlayerInputComponent->BindAxis("LookUpRate", this, &APlayerCharacter::LookUpAtRate);

	// handle touch devices
	PlayerInputComponent->BindTouch(IE_Pressed, this, &APlayerCharacter::TouchStarted);
	PlayerInputComponent->BindTouch(IE_Released, this, &APlayerCharacter::TouchStopped);

	// VR headset functionality
	PlayerInputComponent->BindAction("ResetVR", IE_Pressed, this, &APlayerCharacter::OnResetVR);
}


void APlayerCharacter::BeginPlay()
{
	Super::BeginPlay();

	if (AWeaponBase* Gun = Cast<AWeaponBase> (Weapon->GetChildActor()))
	{
		Gun->AttachToComponent(GetMesh(), FAttachmentTransformRules::SnapToTargetNotIncludingScale, FName(TEXT("hand_rSocket")));
		Gun->SetActorHiddenInGame(true);
	}

	BasedCameraLocation = FollowCamera->GetRelativeLocation();
}

void APlayerCharacter::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	if (bAim)
	{
		LineTraceStart = GetActorLocation() + FVector(0.0f, 0.0f, TraceZOffset);
		LineTraceEnd = LineTraceStart + FollowCamera->GetForwardVector() * TraceLength;
		DrawDebugLine(GetWorld(), LineTraceStart, LineTraceEnd, FColor::Red);
	}

	
}

void APlayerCharacter::FindTargetToKill()
{
	FHitResult EnemyHitResult;
	FCollisionObjectQueryParams ObjectParams;
	ObjectParams.AddObjectTypesToQuery(ECC_Pawn);
	FCollisionQueryParams CollisionParams;
	CollisionParams.AddIgnoredActor(this);
	
	bool DidHit = GetWorld()->LineTraceSingleByObjectType(EnemyHitResult, LineTraceStart, LineTraceEnd, ObjectParams, CollisionParams);
	AEnemyCharacter* TargetToKill = Cast<AEnemyCharacter>(EnemyHitResult.Actor);
	if (TargetToKill != nullptr)
	{
		TargetToKill->ApplyDamage();
	}
}



void APlayerCharacter::OnResetVR()
{
	// If Unreal_Project is added to a project via 'Add Feature' in the Unreal Editor the dependency on HeadMountedDisplay in Unreal_Project.Build.cs is not automatically propagated
	// and a linker error will result.
	// You will need to either:
	//		Add "HeadMountedDisplay" to [YourProject].Build.cs PublicDependencyModuleNames in order to build successfully (appropriate if supporting VR).
	// or:
	//		Comment or delete the call to ResetOrientationAndPosition below (appropriate if not supporting VR)
	UHeadMountedDisplayFunctionLibrary::ResetOrientationAndPosition();
}

void APlayerCharacter::TouchStarted(ETouchIndex::Type FingerIndex, FVector Location)
{
		Jump();
}

void APlayerCharacter::TouchStopped(ETouchIndex::Type FingerIndex, FVector Location)
{
		StopJumping();
}

void APlayerCharacter::TurnAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerYawInput(Rate * BaseTurnRate * GetWorld()->GetDeltaSeconds());
}

void APlayerCharacter::LookUpAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerPitchInput(Rate * BaseLookUpRate * GetWorld()->GetDeltaSeconds());
}

void APlayerCharacter::MoveForward(float Value)
{
	if ((Controller != nullptr) && (Value != 0.0f))
	{
		// find out which way is forward
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		// get forward vector
		FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);
		Value = IsHolding() ? Value * HoldingSpeedScale : Value;
		AddMovementInput(Direction, Value);
	}
}

void APlayerCharacter::MoveRight(float Value)
{
	if ( (Controller != nullptr) && (Value != 0.0f) )
	{
		// find out which way is right
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);
	
		// get right vector 
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);
		Value = IsHolding() ? Value * HoldingSpeedScale : Value;
		// add movement in that direction
		AddMovementInput(Direction, Value);
	}
}


void APlayerCharacter::GrabHand() /*4/20 Code to review*/
{
	if (!bHolding)
	{
		if (SidekickCharacterToGrab)
		{
			SidekickCharacterToGrab->Follow();
			
			bHolding = true;
			
			if (FollowME_SFX)
			{
				UGameplayStatics::PlaySoundAtLocation(this, FollowME_SFX, GetActorLocation());
			}

		}
	}
	else
	{
		if (SidekickCharacterToGrab)
		{
			SidekickCharacterToGrab->Wait();

			bHolding = false;

			/*UGameplayStatics* PlayFollowSound = PlaySound->PlaySound2D() Add play sound through c++ code*/
		}
	}

}



void APlayerCharacter::ThrowRock()
{
	if (RockTemplate!=nullptr)
	{
		if (Rocks > 0 && !bWeaponMode)
		{
			FTransform SpawnPosition = GetActorTransform();
			SpawnPosition.AddToTranslation(GetActorForwardVector()*RockDistanceFromPlayer);
			FRotator PlayerRotation = GetActorRotation();
			PlayerRotation.Add(RockThrowAngle, 0.0f, 0.0f); //Getting player rotation in order to send it to the rock to make sure they have the same rotation
			SpawnPosition.SetRotation(PlayerRotation.Quaternion());
			AActor* RockThrown = GetWorld()->SpawnActor<AActor>(RockTemplate.Get(), SpawnPosition);
			//UGameplayStatics* PlaySound = PlaySound->PlaySound2D(Flare_SFX);
			ConsumeRocks();
		}
		
	}
	
}

void APlayerCharacter::ToggleCrouch()
{
	if (GetCharacterMovement()->IsCrouching())
	{
		UnCrouch();
	}
	else if(bHolding == false)
	{
		Crouch();
	}
}



void APlayerCharacter::ToggleWeapon()
{
	if (bCanUseWeapon)
	{
		
		bWeaponMode = !bWeaponMode;
		OnWeaponEquipped(bWeaponMode);
		Weapon->GetChildActor()->SetActorHiddenInGame(!bWeaponMode);
	}

}

void APlayerCharacter::FireWeapon()
{
	if (AWeaponBase* Gun = Cast<AWeaponBase>(Weapon->GetChildActor())) 
	{
		if (Gun->GetAmmo() > 0 && bWeaponMode)
		{
			
			FindTargetToKill();
			Gun->ConsumeAmmo();
		}
	}



}

void APlayerCharacter::AimWeaponPressed()
{
	SetIsAiming(true);
}

void APlayerCharacter::AimWeaponReleased()
{
	SetIsAiming(false);
}

void APlayerCharacter::SetIsAiming(bool bIsAiming)
{
	if (bWeaponMode)
	{
		bAim = bIsAiming;

		FollowCamera->SetRelativeLocation(bAim ? BasedCameraLocation + AimOverShoulderDisplacement : BasedCameraLocation);

		if (UCharacterMovementComponent* MoveComponent = GetCharacterMovement())
		{
			MoveComponent->bUseControllerDesiredRotation = bAim;
			MoveComponent->bOrientRotationToMovement = !bAim;
		}

		OnAimChanged(bAim);

	}
}

void APlayerCharacter::Reload()
{
	if (bWeaponMode)
	{
		if (AWeaponBase* Gun = Cast<AWeaponBase>(Weapon->GetChildActor()))
		{
			if (bWeaponMode)
			{

				Gun->Reload();
			}
		}
	}
}

void APlayerCharacter::Interact()
{
	if (InteractionPointToInteractWith)
	{
		InteractionPointToInteractWith->UseInteraction();
	}

}

void APlayerCharacter::PauseGame()
{

}

void APlayerCharacter::ConsumeRocks()
{
	if (Rocks > 0)
	{
		Rocks--;
	}
}

