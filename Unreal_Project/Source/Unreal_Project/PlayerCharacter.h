// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "PlayerCharacter.generated.h"

class ASidekickCharacter;
class AEnemyCharacter;
class AInteractionPoint;

UCLASS(config=Game)
class APlayerCharacter : public ACharacter
{
public:
	GENERATED_BODY()

	/** Camera boom positioning the camera behind the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

	/** Follow camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* FollowCamera;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	class UChildActorComponent* Weapon;

	APlayerCharacter();

	/** Base turn rate, in deg/sec. Other scaling may affect final turn rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Camera)
	float BaseTurnRate;

	/** Base look up/down rate, in deg/sec. Other scaling may affect final rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Camera)
	float BaseLookUpRate;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	float TraceLength = 100.0f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	float TraceZOffset = 100.0f;

	UPROPERTY(BlueprintReadOnly)
	bool isCrouched = false;

	UPROPERTY(BlueprintReadWrite)
	bool bCanUseWeapon = false;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	TSoftClassPtr <AActor> RockTemplate;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	TSoftClassPtr <AActor> BulletTemplate;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	float RockDistanceFromPlayer = 10.0f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	float RockThrowAngle = 50.0f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	float FieldOfViewAiming = 60.0f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	float FieldOfViewDefault = 90.0f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	FVector AimOverShoulderDisplacement = FVector(30.0f, 6.0f, 5.0f);

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	float HoldingSpeedScale = 0.5f;

	UFUNCTION(BlueprintImplementableEvent)
	void OnAimChanged(bool bIsAiming);

	UFUNCTION(BlueprintImplementableEvent)
	void OnWeaponEquipped(bool bIsEquipped);

	UFUNCTION(BlueprintCallable) // Holding Sidekick
	bool IsHolding() const { return bHolding; }

	UFUNCTION(BlueprintCallable)
	void ToggleWeapon();

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	USoundBase* FollowME_SFX;

	void GrabHand();

	void ThrowRock();

	void ToggleCrouch();
	

	void FireWeapon();

	void AimWeaponPressed();

	void AimWeaponReleased();

	void Reload();

	void Interact();

	void PauseGame();


	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
	/** Returns FollowCamera subobject **/
	FORCEINLINE class UCameraComponent* GetFollowCamera() const { return FollowCamera; }

	void SetSidekickCharacterToGrab(ASidekickCharacter* Character) { SidekickCharacterToGrab = Character; }

	bool IsSidekickCharacterClose() const { return SidekickCharacterToGrab != nullptr; }

	void SetInteractionPointTarget(AInteractionPoint* IP) { InteractionPointToInteractWith = IP; }

	void ConsumeRocks();

	bool bHolding = false;


protected:

	/** Resets HMD orientation in VR. */
	void OnResetVR();

	/** Called for forwards/backward input */
	void MoveForward(float Value);

	/** Called for side to side input */
	void MoveRight(float Value);

	/** 
	 * Called via input to turn at a given rate. 
	 * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	 */
	void TurnAtRate(float Rate);

	/**
	 * Called via input to turn look up/down at a given rate. 
	 * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	 */
	void LookUpAtRate(float Rate);

	/** Handler for when a touch input begins. */
	void TouchStarted(ETouchIndex::Type FingerIndex, FVector Location);

	/** Handler for when a touch input stops. */
	void TouchStopped(ETouchIndex::Type FingerIndex, FVector Location);

	// APawn interface
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	// End of APawn interface

	virtual void BeginPlay() override;

	virtual void Tick(float DeltaSeconds) override;

	void FindTargetToKill();


	UPROPERTY(BlueprintReadOnly)
	bool bWeaponMode = false;

	UPROPERTY(BlueprintReadOnly)
	bool bAim = false;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	int MaxRocks = 4;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	int Rocks = MaxRocks;

private:
	UPROPERTY(Transient)
	ASidekickCharacter* SidekickCharacterToGrab = nullptr;

	UPROPERTY(Transient)
	AInteractionPoint* InteractionPointToInteractWith = nullptr;

	FVector LineTraceStart = FVector::ZeroVector;

	FVector LineTraceEnd = FVector::ZeroVector;


	bool bRunning = false;

	FVector BasedCameraLocation = FVector::ZeroVector;

	void SetIsAiming(bool bIsAiming);
};

