// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Subsystems/GameInstanceSubsystem.h"
#include "InteractionSubsystem.generated.h"

/**
 * 
 */
UCLASS()
class UNREAL_PROJECT_API UInteractionSubsystem : public UGameInstanceSubsystem
{
	GENERATED_BODY()
	
public:
	static UInteractionSubsystem* Get(const UObject* WorldObject);
};
