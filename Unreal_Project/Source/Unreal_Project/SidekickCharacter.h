// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "SidekickCharacter.generated.h"

class USphereComponent;

UCLASS()
class UNREAL_PROJECT_API ASidekickCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	ASidekickCharacter();

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	
	void Follow();

	void Wait();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	virtual void EndPlay(const EEndPlayReason::Type Reason) override;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		USphereComponent* AttachRangeSphere;

	UFUNCTION() /*4/20 Code to review*/
		void OnAttachRangeBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
			UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult);

	UFUNCTION() /*4/20 Code to review*/
		void OnAttachRangeEndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
			UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

private:
	//bool bIsGrabbed = false; /*4/20 Code to review*/
};
