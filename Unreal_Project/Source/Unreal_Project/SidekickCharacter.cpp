// Fill out your copyright notice in the Description page of Project Settings.


#include "SidekickCharacter.h"
#include "Components/SphereComponent.h"
#include "PlayerCharacter.h"
#include "SidekickController.h"

// Sets default values
ASidekickCharacter::ASidekickCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	
	AttachRangeSphere = CreateDefaultSubobject<USphereComponent>(FName(TEXT("AttachRangeSphere")));
	AddOwnedComponent(AttachRangeSphere);

	AttachRangeSphere->AttachToComponent(RootComponent, FAttachmentTransformRules::KeepRelativeTransform);
}

// Called when the game starts or when spawned
void ASidekickCharacter::BeginPlay()
{
	Super::BeginPlay();
	
	if (AttachRangeSphere)
	{
		AttachRangeSphere->OnComponentBeginOverlap.AddDynamic(this, &ASidekickCharacter::OnAttachRangeBeginOverlap); /*4/20 Code to review*/
		AttachRangeSphere->OnComponentEndOverlap.AddDynamic(this, &ASidekickCharacter::OnAttachRangeEndOverlap);
	}

}

void ASidekickCharacter::EndPlay(const EEndPlayReason::Type Reason)
{
	Super::EndPlay(Reason);
	if (AttachRangeSphere)
	{
		AttachRangeSphere->OnComponentBeginOverlap.RemoveDynamic(this, &ASidekickCharacter::OnAttachRangeBeginOverlap); /*4/20 Code to review*/
		AttachRangeSphere->OnComponentEndOverlap.RemoveDynamic(this, &ASidekickCharacter::OnAttachRangeEndOverlap);
	}

}

// Called every frame
void ASidekickCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void ASidekickCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

void ASidekickCharacter::Follow()
{

	if (ASidekickController* AIController = Cast<ASidekickController>(GetController()))
	{
		AIController->Follow();
	}
}

void ASidekickCharacter::Wait()
{
	if (ASidekickController* AIController = Cast<ASidekickController>(GetController()))
	{
		AIController->Wait();
	}
}

void ASidekickCharacter::OnAttachRangeBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, /*4/20 Code to review*/
	UPrimitiveComponent* OtherComp, int32 OtherBodyIndex,
	bool bFromSweep, const FHitResult & SweepResult)
{
	if (APlayerCharacter* MyCharacter = Cast<APlayerCharacter>(OtherActor))
	{
		MyCharacter->SetSidekickCharacterToGrab(this);
	}

}

void ASidekickCharacter::OnAttachRangeEndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, /*4/20 Code to review*/
	UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	if (APlayerCharacter* MyCharacter = Cast<APlayerCharacter>(OtherActor))
	{
		MyCharacter->SetSidekickCharacterToGrab(nullptr);

	}
}
