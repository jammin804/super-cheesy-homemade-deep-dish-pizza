// Fill out your copyright notice in the Description page of Project Settings.


#include "InteractionResult.h"
#include "InteractionPoint.h"

// Sets default values
AInteractionResult::AInteractionResult()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AInteractionResult::BeginPlay()
{
	Super::BeginPlay();
	for (AInteractionPoint* Point : InteractionPoints)
	{
		if (Point)
		{
			InteractionEvents.Add(Point->OnInteractionEvent.AddUObject(this, &AInteractionResult::OnInteractionPointInteracted));
			InteractionsPressed.Add(false);
		}
		
	}
}

void AInteractionResult::EndPlay(const EEndPlayReason::Type Reason)
{
	Super::EndPlay(Reason);
	for (int i =0; i<InteractionPoints.Num(); ++i)
	{
		AInteractionPoint* Point = InteractionPoints[i];
		Point->OnInteractionEvent.Remove(InteractionEvents[i]);
	}
}

// Called every frame
void AInteractionResult::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (!bInteractionComplete)
	{
		if (InteractionType == EInteractionTypes::Standard)
		{
			for (int i = 0; i < InteractionPoints.Num(); i++)
			{
				if (InteractionsPressed[i] == false)
				{
					return;
				}
			}
			OnInteraction();
			bInteractionComplete = true;
		}
	}
}

void AInteractionResult::OnInteractionPointInteracted(AInteractionPoint* InteractionPoint)
{
	if (InteractionType == EInteractionTypes::Standard)
	{
		for (int i = 0; i < InteractionPoints.Num(); i++)
		{
			if (AInteractionPoint* Point = InteractionPoints[i])
			{
				if (Point == InteractionPoint)
				{
					InteractionsPressed[i] = true;
					break;
				}
			}
			
		}
	}
}