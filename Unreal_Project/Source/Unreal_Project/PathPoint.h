// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "PathPoint.generated.h"

UCLASS()
class UNREAL_PROJECT_API APathPoint : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	APathPoint();

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(BlueprintReadOnly, EditAnywhere)
		float WaitTime = 0.0f;

	UPROPERTY(BlueprintReadOnly, EditAnywhere)
		float WaitDeviation = 0.0f;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;



};
