// Fill out your copyright notice in the Description page of Project Settings.


#include "EnemyController.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "PlayerCharacter.h"
#include "Kismet/GameplayStatics.h"

void AEnemyController::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	

}

void AEnemyController::OnPerceptionUpdated(AActor* Actor, const FAIStimulus& Stimulus)
{
	static FName PlayerTag(TEXT("Player"));
	static FName ThrowableTag(TEXT("Throwable"));
	static FName SideKickTag(TEXT("Sidekick"));

	static FName HasLineOfSight(TEXT("HasLineOfSight"));
	static FName EnemyActor(TEXT("TargetLocationActor"));

	if (Actor->ActorHasTag(ThrowableTag))
	{
		
		if (Stimulus.WasSuccessfullySensed())
		{
			Blackboard->SetValueAsBool(HasLineOfSight, true);
			Blackboard->SetValueAsObject(EnemyActor, Actor);
		}
		else
		{
			Blackboard->SetValueAsBool(HasLineOfSight, false);
			Blackboard->SetValueAsObject(EnemyActor, nullptr);
		}
	}

	else if (Actor->ActorHasTag(PlayerTag) || Actor->ActorHasTag(SideKickTag))
	{
		if (AActor* CurrentEnemy = Cast<APlayerCharacter>(Blackboard->GetValueAsObject(EnemyActor)))
		{
			if (CurrentEnemy->ActorHasTag(ThrowableTag))
			{
				return;
			}
			
		}
		
		if (Stimulus.WasSuccessfullySensed())
		{
			Blackboard->SetValueAsBool(HasLineOfSight, true);
			Blackboard->SetValueAsObject(EnemyActor, Actor);
		}
		else
		{
			Blackboard->SetValueAsBool(HasLineOfSight, false);
			Blackboard->SetValueAsObject(EnemyActor, nullptr);
		}

	}
	

}

void AEnemyController::OnPossess(APawn* InPawn)
{
	Super::OnPossess(InPawn);
	if (BehaviorTree)
	{
		RunBehaviorTree(BehaviorTree);
	}

}
