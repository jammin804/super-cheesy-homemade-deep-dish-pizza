// Fill out your copyright notice in the Description page of Project Settings.


#include "WeaponBase.h"
#include "Components/SphereComponent.h"
#include "UObject/NameTypes.h"
#include "PlayerCharacter.h"

// Sets default values
AWeaponBase::AWeaponBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;
	WeaponAttached = CreateDefaultSubobject<USphereComponent>(FName(TEXT("WeaponAttached")));
	AddOwnedComponent(WeaponAttached);
}

// Called when the game starts or when spawned
void AWeaponBase::BeginPlay()
{
	Super::BeginPlay();
	
	MaxRefillAmmo = MaxAmmo;

	OnActorBeginOverlap.AddDynamic(this, &AWeaponBase::OnWeaponDraw);
}

void AWeaponBase::EndPlay(const EEndPlayReason::Type e)
{
	OnActorBeginOverlap.RemoveDynamic(this, &AWeaponBase::OnWeaponDraw);
}

// Called every frame
void AWeaponBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AWeaponBase::ConsumeAmmo()
{
	if (Ammo > 0)
	{
		Ammo--;
	}
}

void AWeaponBase::Reload()
{
	if (Ammo != ClipSize)
	{
		if (MaxAmmo > ClipSize)
		{
			Ammo = ClipSize;
			MaxAmmo -= ClipSize;
		}
		else
		{
			Ammo = MaxAmmo;
			MaxAmmo = 0;
		}
	}
}

void AWeaponBase::PickupAmmo(int AmountOfAmmo)
{
	MaxAmmo += AmountOfAmmo;
	MaxAmmo = FMath::Clamp(MaxAmmo, 0, MaxRefillAmmo);
}

void AWeaponBase::OnWeaponDraw(AActor* OverlappedActor, AActor* OtherActor)
{
	/*if (APlayerCharacter* Player = Cast<APlayerCharacter>(OtherActor))
	{
		WeaponAttached->AttachToComponent(Player->GetMesh(), FAttachmentTransformRules::KeepRelativeTransform, FName(TEXT("hand_rSocket")));
	}*/

}

