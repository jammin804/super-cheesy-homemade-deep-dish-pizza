// Fill out your copyright notice in the Description page of Project Settings.


#include "EnemyUIWidget.h"
#include "EnemyUI.h"

void UEnemyUIWidget::SetHealthBarPct(float HealthBarPercentage)
{
    if (UEnemyUI* EnemyUI = Cast<UEnemyUI>(GetWidget()))
    {
        EnemyUI->SetHealthBarPercentage(HealthBarPercentage);
    }
}
